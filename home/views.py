from django.shortcuts import render

def index(request):
    return render(request, 'home/index.html')
def more(request):
    return render(request, 'home/more.html')

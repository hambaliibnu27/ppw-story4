from django.shortcuts import render
import requests
from django.http import HttpResponse
import json

# Create your views here.

def index(request):
    return render(request, 'search/index.html')

def retrieve(request, book):
    req = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + book)
    data = req.json()
    data = json.dumps(data)
    print(data)
    return HttpResponse(content = data, content_type="application/json")
$("#keyword").keyup(function() {

    var isi_key = $("#keyword").val();

    // Buat ngisi keyword sesuai yang ditulis.
    var url_dipanggil = "api/books/" + isi_key;

    $.ajax({
        url: url_dipanggil,
        success: function(hasil) {
            console.log(hasil.items);
            var obj_hasil = $("#hasil");
            obj_hasil.empty();

            for (i = 0; i < hasil.items.length; i++) {
                var title = hasil.items[i].volumeInfo.title;
                var thumbnails = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                var prev_links = hasil.items[i].volumeInfo.previewLink;
                console.log(title);
                obj_hasil.append('<tr><td><a href="' + prev_links + '">' + title + '</a>' + '</td> <td><img src=' + thumbnails + '></td></tr>');

            }
        }
    });

});
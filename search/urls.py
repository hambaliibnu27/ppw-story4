from django.urls import path

from . import views

app_name = 'search'

urlpatterns = [
    path('', views.index, name='index'),
    path('api/books/<str:book>/', views.retrieve, name='retrieve')
    
]
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class DataProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=300)
    photo = models.CharField(max_length=500)
    birthDate = models.DateField()
    city = models.CharField(max_length=50)
    def __str__(self):
        return self.name

from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .views import *
import time


class ModelTest(TestCase):
    
    def setUp(self):
        self.user = User.objects.create(
            username = 'bobo',
            password = 'hunterxhunter',
        )
        self.dataUser = DataProfile.objects.create(
            user = self.user,
            name = 'bobo si bobo',
            photo = 'https://assets.kompasiana.com/items/album/2018/09/03/sasasas-5b8d5b6ac112fe4c7512f733.jpg?t=o&v=740&x=416',
            birthDate = '2000-09-05',
            city = 'Jakarta'
        )

    def test_instance_created(self):
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(DataProfile.objects.count(), 1)
        self.assertEqual(str(self.dataUser), "bobo si bobo")
        

class UrlsTest(TestCase):

    def setUp(self):
        self.index = reverse("authentication:index")
        self.login = reverse("authentication:loginPage")
        self.register = reverse("authentication:registerPage")
        self.logout = reverse("authentication:logoutPage")
    
    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

    def test_login_use_right_function(self):
        found = resolve(self.login)
        self.assertEqual(found.func, loginPage)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, registerPage)

    def test_logout_use_right_function(self):
        found = resolve(self.logout)
        self.assertEqual(found.func, logoutPage)


class ViewsTest(TestCase):
    
    def setUp(self):
        self.client = Client()
        self.index = reverse("authentication:index")
        self.login = reverse("authentication:loginPage")
        self.register = reverse("authentication:registerPage")
        self.logout = reverse("authentication:logoutPage")

    def test_GET_index_no_data(self):
        # If no one login it redirect to login page
        response1 = self.client.get(self.index, follow=True)
        self.assertEqual(response1.status_code, 200)
        self.assertIn("Login", str(response1.content))
        # If a user login it show her name
        user = User.objects.create(username='kucing')
        user.set_password('12345')
        user.save()
        self.client.login(username='kucing', password='12345')        
        response2 = self.client.get(self.index)
        self.assertEqual(response2.status_code, 200)
        self.assertTemplateUsed(response2, "authentication/index.html")
        self.assertIn("kucing", str(response2.content))

    def test_GET_index_data_exist(self):
        # If no one login it redirect to login page
        response1 = self.client.get(self.index, follow=True)
        self.assertEqual(response1.status_code, 200)
        self.assertIn("Login", str(response1.content))
        # If a user login it show her name
        user = User.objects.create(username='kucing')
        user.set_password('12345')
        user.save()
        self.client.login(username='kucing', password='12345')    
        dataUser = DataProfile.objects.create(
            user = user,
            name = 'kucing si kucing',
            photo = 'https://i.ya-webdesign.com/images/cartoon-faces-png-7.png',
            birthDate = '2001-03-21',
            city = 'Jepara'
        )    
        response2 = self.client.get(self.index)
        self.assertEqual(response2.status_code, 200)
        self.assertTemplateUsed(response2, "authentication/index.html")
        self.assertIn("kucing si kucing", str(response2.content))

    def test_GET_register(self):
        response = self.client.get(self.register)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "authentication/register.html")
        self.assertIn("register", str(response.content))


    def test_POST_register_invalid(self):
        response = self.client.post(self.register, {
            'username': 'hambaliibnu27',
            'password1': 'ha',
            'password2': 'ha'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "authentication/register.html")
        self.assertIn("This password is too short", str(response.content))

    def test_POST_register_valid(self):
        response = self.client.post(self.register, {
            'username': 'hambaliibnu27',
            'password1': 'qwertyiopuh67',
            'password2': 'qwertyiopuh67'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "authentication/login.html")
        self.assertIn("Your account created successfully, please login", str(response.content))

    def test_GET_login(self):
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "authentication/login.html")
        self.assertIn("Selamat Datang!", str(response.content))

    def test_POST_login_invalid(self):
        response = self.client.post(self.login, {
            'username': 'hambaliibnu27',
            'password': 'passwordsalah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "authentication/login.html")
        self.assertIn("Username or password is incorrect", str(response.content))

    def test_POST_login_valid(self):
        user = User.objects.create(username='hambaliibnu27')
        user.set_password('qwertyiopuh67')
        user.save()
        response = self.client.post(self.login, {
            'username': 'hambaliibnu27',
            'password': 'qwertyiopuh67',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "authentication/index.html")
        self.assertIn("hambaliibnu27", str(response.content))

    def test_GET_logout(self):
        # If a user login, this view should logout the user and redirect to loginpage
        user = User.objects.create(username='kucing')
        user.set_password('12345')
        user.save()
        self.client.login(username='kucing', password='12345')      
        response = self.client.get(self.logout, follow=True) 
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "authentication/login.html")
        self.assertIn("Logout successfully", str(response.content))
